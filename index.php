<?php

require_once("default/conexion.php");

$login = new Login();


if ($login->isUserLoggedIn() == true) {

   header("location: views/dashboard/");

} else {
 define('RUTA','http://localhost/SCRD/'); 
    ?>

<!DOCTYPE html>
<html lang="es" >
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale-1, shrink-to-fit-no">
	<title>Bienvenidos | Fundacion CYA</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="librerias/css/estilos.css">

</head>
<body style="background-image: url(recursos/fondologin.png); background-repeat: no-repeat; background-size: cover; background-position: center; background-attachment: fixed;"  >
	
	<div id="login">
		
		<!--<h3 class="text-center display-6">Fundacion Concienccia y amor sin fronteras</h3>-->
				
		<div class="container ">
			<div id="login-row" class="row justify-content-center align-items-center " >
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12 bg-light text-dark">

		<!-- FORMULARIO A DOS COLUMNAS -->				
		<div class="box" >
						<div class="circle" style="background-color: #6ed232">
							<div class="text-out-sup">Fundación Conciencia y Amor</div>
						</div>
						
					
            <div class="left">
                <div class="content">
                    <div class="text-center"><img class="logo-img" src="recursos/logo.png"></div>
                </div>
            </div>
            <div class="right">
                <div class="content">

                   <form id="formlogin" class="form" action="index.php" method="POST">
						
							<div class="form-group">
								<label for="usuario" class="text-dark">Nombre de usuario</label>
								<input type="text" name="usuario" id="usuario" class="boton">
							</div>
							<div class="form-group">
								<label for="password" class="text-dark">Contraseña</label>
								<input type="password" name="password" id="password" class="boton">
							</div>
							                 
							<div class="text-center" >
								<button type="submit" name="login" class="button btn-sm" value="Ingresar">Ingresar</button> 
								<div class="text-out-inf"><a class="forgot_pass" href="">Recuperar contraseña</a></div>
							</div>

						</form>

                </div>

            </div>

        </div>
        <?php if (isset($login)): ?>
                  		<?php if ($login->errors): ?>
                  			<div class="alert alert-danger text-center" role="alert" style="margin-top: 15px;">
 							<strong>ERROR!</strong>&nbsp;<?php foreach ($login->errors as $error){echo $error;} ?>	
							</div>
                  		<?php endif ?>              
                  	<?php endif ?>
        <!-- FIN FORMULARIO A DOS COLUMNAS -->		

					</div>

				</div>
			</div>
		</div>
	
	</div>

<script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="popper/popper.min.js"></script>

<script type="text/javascript" src="plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="codigo.js"></script>
</body>
</html> 

<?php } ?>