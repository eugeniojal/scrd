<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])){ 
$usuario = $_SESSION['user_id']; 

}else{
  // header("location: httsps://127.0.0.1/scrd/");
   header("location: ../../");

}
    
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistema Web</title>
        <link rel="icon" href="../../recursos/logo-fundacion.png" type="image/x-icon">
        <!--ESTILOS DEL MENU-->
        <link href="../../librerias/css/styles2.css" rel="stylesheet" />
       <!--ESTILOS DE PANTALLAS INTERNAS-->
        <link rel="stylesheet" type="text/css" href="../../librerias/css/estilos.css">
        
        <link rel="stylesheet" type="text/css" href="../../librerias/css/sb-admin-2.css">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/    alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
       

       <!--BOOTSTRAP menu responsive-->
       <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
       -->

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.24/af-2.3.6/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/sc-2.0.3/sb-1.0.1/sp-1.2.2/datatables.min.css"/>

        <!-- SCRIPT PARA ICONOS FONTAWESOME DEL MENU-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.html">FUNDACION CYA</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <!--<form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                <div class="input-group-append">
                <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                </div>
                </div>
            </form>-->
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Configuración</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
                                    Salir
                                </a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <a class="nav-link" href="../dashboard/"
                            ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Vista general</a
                            >
                            
                            
                                
                                <div class="sb-sidenav-menu-heading">Funciones</div>
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                    Pacientes
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                    ></a>

                                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                      
                                        <nav class="sb-sidenav-menu-nested nav" href="#">
                                       
                                            <a class="nav-link" href="../Pacientes">
                                                <i class="fas fa-user-plus"></i> &nbsp; Nuevo paciente</a> 
                                           
                                            <a class="nav-link" href="layout-sidenav-light.html"><i class="far fa-id-card"></i> &nbsp;&nbsp;  Ver ficha</a></nav>



                                    </div>

                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages"
                                    ><div class="sb-nav-link-icon"><i class="fas fa-hand-holding-heart"></i></div>
                                        Donaciones
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                        ></a>

                                       <!-- <i class="fas fa-hand-holding-medical"></i>-->




                                        <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                            <nav class="sb-sidenav-menu-nested nav" href="#">
                                       
                                            <a class="nav-link" href="layout-static.html">
                                                <i class="fas fa-plus-circle"></i> &nbsp; Nueva donación</a> 
                                           
                                            <a class="nav-link" href="layout-sidenav-light.html"><i class="fas fa-file-invoice-dollar"></i> &nbsp;&nbsp; Hist. de donaciones</a>

                                            <a class="nav-link" href="layout-sidenav-light.html"><i class="far fa-clock"></i> &nbsp;&nbsp; Don. pendientes</a></nav>
                                        </div>
                                        
                         
                            
                            <div class="sb-sidenav-menu-heading">Gráficas</div>
                            <a class="nav-link" href="charts.html"
                            ><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Estadísticas</a
                                ><a class="nav-link" href="tabla.php"
                                ><div class="sb-nav-link-icon"><i class="fas fa-print"></i></div>
                                    Reportes</a
                                >
                            </div>
                    </div>
                   
                </nav>
            </div>
            <div id="layoutSidenav_content">
            </div>
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¿Estas seguro que deseas salir?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Selecciona "Salir" si quieres cerrar tu sesión actual.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" >Cancelar</button>
                    <a class="btn btn-primary" href="http://localhost/scrd/index.php?logout">Salir</a>
                </div>
            </div>
        </div>
    </div>

        </div>
         
       
      

       <!--SCRIPT SUBMENU DESPLEGABLES -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

       <!--SCRIPT ANIMACION MENU-->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="../../librerias/js/scripts2.js"></script>

        <!--

       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="../../librerias/js/chart-area-demo.js"></script>
        <script src="../../librerias/js/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="../../librerias/js/datatables-demo.js"></script>
      -->
    </body>
</html>