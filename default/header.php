<?php session_start();
if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
    $usuario = $_SESSION['user_id'];
    $nombre = $_SESSION['nombre'];
    $nivel = $_SESSION['nivel'];

} else {
    // header("location: httsps://127.0.0.1/scrd/");
    header("location: ../../");

}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../recursos/logo-fundacion.png" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<!-- Bootstrap CSS -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<!-- ICONOS -->
<!-- FONT AWESOME -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
<!-- ICONOS -->

<!-- MODAL -->


  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- ALERTIFY -->
<!-- JavaScript -->
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../../librerias/css/estilos.css">
<!-- <link rel="stylesheet" type="text/css" href="../../librerias/css/sb-admin-2.min.css"> -->
<link rel="stylesheet" type="text/css" href="../../librerias/css/sb-admin-2.css">
<!-- <link rel="stylesheet" type="text/css" href="../../librerias/css/all.min.css"> -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
<!--ESTILOS DEL MENU-->
        <link href="../../librerias/css/styles2.css" rel="stylesheet" />
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.24/af-2.3.6/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/sc-2.0.3/sb-1.0.1/sp-1.2.2/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.24/af-2.3.6/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/sc-2.0.3/sb-1.0.1/sp-1.2.2/datatables.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
     <!-- <script src="../../librerias/js/scripts"></script> -->


    <!-- Custom scripts for all pages-->


<!-- SELECT2 -->

    <title>FUNDACIÓN CYA</title>
  </head>
 <body class="sb-nav-fixed">

    <!--TOPBAR-->
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand">FUNDACION CYA</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <ul class="navbar-nav ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $nombre ; ?><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Configuración</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
                                    Salir
                                </a>
                    </div>
                </li>
            </ul>
        </nav>

<!--MODAL ALERTA DE CIERRE DE SESION-->
 <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¿Estas seguro que deseas salir?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Selecciona "Salir" si quieres cerrar tu sesión actual.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal" >Cancelar</button>
                    <a class="btn btn-primary" href="../../index.php?logout">Salir</a>
                </div>
            </div>
        </div>
    </div>


<!--NAV MENU LATERAL-->

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav  sidebar sidebar-dark " id="accordionSidebar">
            <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <a class="nav-link" href="../dashboard/"
                            ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i> &nbsp;Vista general</a
                            >



                                <div class="sb-sidenav-menu-heading">Funciones</div>
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div><i class="fas fa-users"></i></div> &nbsp;Pacientes<div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                    ></a>

                                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                        <nav class="sb-sidenav-menu-nested nav" href="#">
                                            <?php if ($nivel == 5): ?>
                                                 <a class="nav-link" href="../pacientes">
                                                <i class="fas fa-user-plus"></i> &nbsp; Nuevo paciente</a>
                                            <?php endif ?>
                                           
                                            <a class="nav-link" href="../DetallePaciente"><i class="far fa-id-card"></i> &nbsp;&nbsp;Pacientes registrados</a></nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages"
                                    ><div class="sb-nav-link-icon"><i class="fas fa-hand-holding-heart"></i></div>
                                        Donaciones
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                        ></a>
                                        <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                            <nav class="sb-sidenav-menu-nested nav" href="#">



                                            <a class="nav-link" href="../donaciones"><i class="fas fa-file-invoice-dollar"></i> &nbsp;&nbsp; Donaciones</a>

                                            <a class="nav-link" href="../donacionesp"><i class="far fa-clock"></i> &nbsp;&nbsp; Pendiente</a>

                                            <a class="nav-link" href="../donacionese"><i class="fas fa-check-square"></i> &nbsp;&nbsp; Entregado</a>

                                        </nav>
                                        </div>



  <!-- Content Wrapper 
                                        <nav class="" id="sidenavAccordionPages">
                                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth"
                                                ><i class="fas fa-diagnoses"></i>&nbsp;&nbsp;Estudios
                                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                                                    ></a>
                                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                                        <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="login.html"><i class="fas fa-plus-circle"></i>&nbsp;&nbsp;Nuevo estudio</a><a class="nav-link" href="register.html"><i class="fas fa-stream"></i>&nbsp;&nbsp;Status de estudios</a></nav>
                                                    </div>
                                                </nav>



                                         <div class="sb-sidenav-menu-heading">Gráficas</div>
                            <a class="nav-link" href="charts.html"
                            ><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Estadísticas</a>-->
                                    <a class="nav-link" href="../reportes/">
                                    <div class="sb-nav-link-icon"><i class="fas fa-print"></i></div>
                                    Reportes</a
                                >
                            </div>



        </ul>
  <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->

          <div id="container-fluid">
            <div id="container-fluid"">

            <!-- <div id="container-fluid" style="margin-top: 50px; margin-left: 20px;">

            <div id="container-fluid" style="margin:120px;"> -->

        <!-- End of Sidebar -->





       <!--SCRIPT ANIMACION MENU-->

        <script src="../../librerias/js/scripts2.js"></script>
