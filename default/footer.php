    <!-- Optional JavaScript; choose one of the two! -->
</div>
    <!-- Option 1: Bootstrap Bundle with Popper -->

<footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        
                        <span>Copyright &copy; <img src="../../recursos/logoceg.png" alt="" width="20" height="20" class="d-inline-block align-text-top"> Corporación C.E.G - Desarrollo de soluciones informaticas a la medida</span>
                    </div>
                </div>
            </footer>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    -->
     </div>
  </body>
</html>