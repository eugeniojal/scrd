<?php
include '../../default/header.php';
?>


                        <h3 class="mt-4">Panel de administración</h3>
                         <div class="row" >
    <!--  <div class="row" style="margin-left: -10px; margin-right: 20px"> -->

<!-- BOTON PACIENTES ACTIVOS -->
                                    <div class="col-xl-3 col-md-6">
                                <div class="card_de text-white mb-4" style="background-color: #26ce4c; margin-top: 30px">

                                     <table>
                                <tr>
                                <td style="width: 30%; text-align: center;"><i class="far fa-address-book" style="font-size: 35px;  margin: 0px "></i></td>
                                <td style="width: 50%; "> Pacientes activos &nbsp;&nbsp;&nbsp; </td>
                                <td style="width: 20%; font-size: 50px; text-align: center;"><strong><label id="pa"></label></strong></td>
                                 </tr>
                                    </table>

                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <!-- <a class="small text-white stretched-link" href="#">Ver detalles</a> -->
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
<!-- BOTON DONACIONES ENTREGADAS -->
                            <div class="col-xl-3 col-md-6">
                                <div class="card_do text-white mb-4" style="background-color: #2196f3; margin-top: 30px">
                            <table>
                                <tr>
                                <td style="width: 30%; text-align: center;"><i class="fas fa-hand-holding-medical" style="font-size: 35px;  margin: 0px "></i></td>
                                <td style="width: 50%; "> Donaciones entregadas </td>
                                <td style="width: 20%; font-size: 50px; text-align: center;"><strong><label id="de"></label></strong></td>
                                 </tr>
                             </table>

                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <!-- <a class="small text-white stretched-link" href="../donaciones/">Ver detalles</a> -->
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>

<!-- BOTON PACIENTES REGISTRADOS -->

                            <div class="col-xl-3 col-md-6">
                                <div class="card_re text-white mb-4" style="background-color: #ff9800; margin-top: 30px">

                                    <table>
                                <tr>
                                <td style="width: 30%; text-align: center;"><i class="far fa-id-card" style="font-size: 35px;  margin: 0px "></i></td>
                                <td style="width: 50%; "> Pacientes registrados </td>
                                <td style="width: 20%; font-size: 50px; text-align: center;"><strong><label id="dp"></label></strong></td>
                                 </tr>
                                    </table>


                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <!-- <a class="small text-white stretched-link" href="../detallepaciente">Ver detalles</a> -->
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>

<!-- BOTON ESTUDIOS -->
                            <div class="col-xl-3 col-md-6" >
                                <div class="card_pa  text-white mb-4" style="background-color: #f44336; margin-top: 30px">

                                     <table>
                                <tr>
                                <td style="width: 30%; text-align: center;"><i class="fas fa-user-plus" style="font-size: 35px;  margin: 0px "></i></td>
                                <td style="width: 50%; "> Estudios asignados</td>
                                <td style="width: 20%; font-size: 50px; text-align: center;"><strong><label id="es"></label></strong></td>
                                 </tr>
                                    </table>

                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <!-- <a class="small text-white stretched-link" href="../pacientes/">Ver detalles</a> -->
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>

                        </div>

<div class="row">
    <div class="col">
                        <h3 class="mt-4">Estadistica Anual</h3>

  <canvas id="myChart" height="100"></canvas>

    </div>
</div>
<div class="row">
                        <h3 class="mt-4" >Estadisticas del mes</h3>

    <div class="col">

         <h4 >Ingreso/Salida de pacientes</h4>
  <canvas id="myChart1"  ></canvas>

    </div>
        <div class="col">
                        <h4 >Donaciones entregadas/pendientes</h4>

  <canvas id="myChart2"></canvas>

    </div>
</div>

<script src="../../librerias/js/js_das.js" type="text/javascript" charset="utf-8" async defer></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<?php
require_once "../../default/footer.php";
?>