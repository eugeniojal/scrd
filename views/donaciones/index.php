<?php
include '../../default/header.php';
?>

<h3 class="mt-4">Donaciones </h3>

<div class="row" id="btn-donaciones">

<div class="col ">
    <!-- Button trigger modal -->
    <?php if ($nivel == 5): ?>
      <button type="button" class="btn btn-success float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">
<i class="fas fa-plus"></i>&nbsp;Nueva donación
</button>
    <?php endif ?>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar donación</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

<div class="modal-body">

<form class="row g-3 needs-validation"  id="nueva_do" method="POST" action="../../api/GuardarDonacion.php">
        <!-- FORMULARIO -->
  <div class="col-md-6">
    <label for="mySelect2" class="form-label">Niño</label>
   <select style="width: 100%" class="js-example-basic-single boton" id="mySelect2" name="mySelect2"></select>
  </div>

  <div class="col-md-6">
    <label for="aporte_do" class="form-label">Aporte</label>
    <input type="text" class="boton" id="aporte_do" name="aporte_do" required>
  </div>

  <div class="col-md-6">
    <label for="costo_do" class="form-label">Costo</label>
    <input type="text" class="boton" id="costo_do" name="costo_do" required>
  </div>

    <div class="col-md-6">
    <label for="fecha_do" class="form-label">Fecha</label>
    <input type="date" class="boton" id="fecha_do" name="fecha_do" required>
  </div>
    <div class="col-md-6">
    <label for="Estado_do"  class="form-label">Estado</label>
    <select id="Estado_do" class="boton" name="Estado_do">
      <option value="1" style="background-color: green;">ENTREGADO</option>
      <option value="0" style="background-color: yellow;">PENDIENTE</option>
    </select>
      </div>

    </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button  class="btn btn-success" id="guardar_pa" type="submit">Guardar</button>
      </div>
      </form>
      <!-- /FORMULARIO -->
    </div>
  </div>
</div>
</div>

</div>



<!-- TABLA DE DONACIONES -->
<br>
<div class="row" id="tb-donaciones">
<div class="card ">




<table id="TablaDonaciones" class="table table-striped ">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Niño</th>
                <th>Aporte</th>
                <th>Costo</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Fecha</th>
                <th>Niño</th>
                <th>Aporte</th>
                <th>Costo</th>
                <th>Estatus</th>
            </tr>
        </tfoot>
    </table>
</div>

</div>
<br>
<br>
<br>
<script src="../../librerias/js/js_do.js" type="text/javascript" charset="utf-8" async defer></script>
<?php
include '../../default/footer.php';
?>