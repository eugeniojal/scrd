<?php include '../../default/header.php';?>
<h3 class="mt-4">Impresión de reportes</h3>
<center>
	<label>Selecciona el tipo de reporte para imprimir</label>
	<div class="row" style="margin: 20px;">
	<select name="reporte" class="boton" id="reporte">
		<option value=""></option>
		<option value="1">Donaciones Entregadas</option>
		<option value="2">Donaciones Pendientes</option>
		<option value="3">Donaciones a paciente</option>
		<option value="4">Donaciones por fecha</option>
		<option value="5">Pacientes Activos</option>
		<option value="6">Pacientes Inactivos</option>
		<option value="7">Pacientes por fecha </option>
	</select>
	</div>
	<div class="row" style="margin: 20px;" id="paciente" hidden>
   <select style="width: 100%" class="js-example-basic-single boton" id="mySelect2" name="mySelect2" ></select>
	</div>
	<div class="row" style="margin: 20px;" hidden id="fecha">
		<div class="col-md-6">
			<label for="desde"></label>
			Desde:
			<input type="date" class="boton" name="" id="desde" value="" placeholder="">
		</div>
		<div class="col-md-6">
			<label for="hasta"></label>
			Hasta:
			<input type="date" class="boton"  name="" id="hasta" value="" placeholder="">
		</div>
	</div>
<div class="row"  style="margin: 20px;">
		<button type="button" id="imprimir" class="btn btn-danger ">
<i class="fas fa-print"></i>&nbsp;IMPRIMIR
	</button>
</div>

</center>
<script src="../../librerias/js/js_re.js" type="text/javascript" charset="utf-8" async defer></script>
<?php include '../../default/footer.php';?>