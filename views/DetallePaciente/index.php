<?php
require_once "../../default/header.php";

if (isset($_REQUEST['paciente']) || !empty($_REQUEST['paciente'])) {
    $paciente = $_REQUEST['paciente'];
    ?>

<input type="hidden" id="id_paciente" name="id_paciente" value="<?php echo $paciente; ?>">



<h3 class="mt-4"">Ficha del paciente</h3>
<div class="row" id="card_detalle">
<div class="card" >
  <div class="rowdetallespa">


  <div class="col ">

    <div class="row " >
    <!-- DATOS DEL PACIENTE -->
      <div class="col">
        <div class="row justify-content-center">
        <i class="fas fa-hospital-user fa-6x text-center" style="margin-top: 20px; margin-bottom: 20px"></i>
        </div>
        <div class="row justify-content-center" id="nombre_pa">

        </div>
        <div class="row justify-content-center"  id="fecha_pa"  >

        </div>
          <div class="row justify-content-center" style="margin-bottom: 20px" id="peso_pa"  >

        </div>
      </div>
        <hr>
      <!-- DATOS DEL REPRESENTANTE -->
      <div class="col">
      <!-- <div class="col-6 "> -->
        <div class="row">
                    <center>
            <h5 class="titulo_detalle" style=" color: #007bff; ">Datos del representante</h5>
            <br>
          </center>

          <!-- <br> -->
        </div>

          <div class="row">
            <div id="nombre_re">


            </div>


          </div>

          <div class="row" style="padding-bottom: 2px">
            <div id="cedula_re">

            </div>
          </div>

          <div class=" row" style="padding-bottom: 2px">
            <div id="tlf_re">

            </div>
          </div>

          <div class=" row"style="padding-bottom: 2px">
            <div id="dir_re">

            </div>
          </div>
        <br>

      </div>
      <div class="col">
      <!-- <div class="col-6"> -->
      <!-- <div class="col-6"> -->
        <div class="row ">
          <center>
            <h5 class="titulo_detalle" style=" color: #007bff; ">Detalles del paciente</h5>
            <br>
          </center>

        </div>
        <div class="row " style="padding-bottom: 2px">
          <div id="area_pa">

          </div>
        </div>
        <div class="row " style="padding-bottom: 2px">
          <div id="diag_pa">

          </div>

        </div>
        <br>

        <div class="row ml-2 " >
          <div class="col justify-content-center">
          <div  id="estado_pa">
            <br>

    <div class="form-check form-switch">
      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked="">
      <label class="form-check-label" for="flexSwitchCheckDefault" id="text"></label>
    </div>
    <br>
          </div>
          </div>




        </div>
  </div>


    </div>
<!-- DETALLES DEL PACIENTE -->
  </div>







  </div>
</div>

</div>

<p></p>
<div class="card" >
  <div class="rowdetallespa">
  <div class="col ">
    <div class="row " >

        <!--   COLUMNA 1 -->
        <div class="col" >
        <div class="row" >
           <p></p>
                    <center>
            <h5 class="titulo_detalle">Donaciones<h5>
            <br>
          </center>
        </div>
                                                                          <!-- inicio de boton -->
          <div class="row">
            <?php if ($nivel == 5): ?>
                        <button type="button" class="btn btn-success float-end" data-bs-toggle="modal" data-bs-target="#exampleModal" id="btn-donacion">
          <i class="fas fa-plus"></i>&nbsp;Nueva donación</button>
            <?php endif ?>

<!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar donación</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form class=""  id="nueva_do" method="POST" action="../../api/GuardarDonacion.php">
        <div class="modal-body">
        <!-- FORMULARIO -->
        <div class="col">
        <input type="hidden" id="mySelect2" name="mySelect2" value="<?php echo $paciente; ?>" required>
        <label for="aporte_do" class="form-label">Aporte</label>
        <input type="text" class="boton" id="aporte_do" name="aporte_do" required>
        </div>
        <div class="col">
        <label for="costo_do" class="form-label">Costo</label>
        <input type="text" class="boton mask" id="costo_do" name="costo_do" required>
        </div>
        <div class="col">
        <label for="fecha_do" class="form-label">Fecha de entrega</label>
        <input type="date" class="boton" id="fecha_do" name="fecha_do" required></div>
        <div class="col">
        <label for="Estado_do"  class="form-label">Estado</label>
        <select id="Estado_do" class="boton" name="Estado_do">
        <option value="1" style="background-color: green;">ENTREGADO</option>
        <option value="0" style="background-color: yellow;">PENDIENTE</option>
        </select>
      </div>
         </div>
         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button  class="btn btn-success" id="guardar_pa" type="submit">Guardar</button>
          </div>
        </form>
      <!-- /FORMULARIO -->
        </div>
      </div>
    </div>
    </div>
                                                                                <!-- findeboton -->
<p></p>
                                                                                <!-- inicio de tabla -->
      <table id="TablaDonaciones" class="table table-striped ">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Aporte</th>
                <th>Costo</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Fecha</th>
                <th>Aporte</th>
                <th>Costo</th>
            </tr>
        </tfoot>
    </table>
                                                                                    <!-- fin de tabla -->

      </div>

    <!--   COLUMNA 2 -->
      <div class="col" >
        <div class="row" >
           <p></p>
                    <center>
            <h5 class="titulo_detalle">Estudios<h5>
            <br>
          </center>
        </div>
                                                                                    <!-- boton -->
          <div class="row">
            <?php if ($nivel == 5): ?>
                   <button type="button" class="btn btn-success float-end" data-bs-toggle="modal" data-bs-target="#exampleModal1" id="btn-estudio">
          <i class="fas fa-plus"></i>&nbsp;Nuevo Estudio</button>
            <?php endif ?>
     

          <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog  modal-dialog-centered">
         <div class="modal-content">
        <form  id="nueva_es" method="POST" action="../../api/GuardarEstudio.php">
         <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo estudio</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div class="row">
        <div class="col">
     <label for="Estudio" class="form-label">Estudio</label>
       <input type="text" class="boton" id="Estudio" name="Estudio" required>
      </div>
      </div>
       <div class="row">
    <div class="col-6">
    <label for="costo_es" class="form-label">Costo</label>
    <input type="text" class="boton mask" id="costo_es" name="costo_es" required>
    </div>
            <div class="col-6">
    <label for="estado_es" class="form-label">Estado</label>
    <select name="estado_es" class="boton" id="estado_es" required>
      <option value=""></option>
      <option style="background-color: #e8053b;" value="1">Pendiente</option>
      <option style="background-color: #ffb509;" value="2">En proceso</option>
      <option style="background-color: #6ed232;" value="3">Realizado</option>
    </select >
   </div>
       </div>
    <div class="row">
      <div class="col-6">
    <label for="fecha_es" class="form-label">Fecha de inicio</label>
    <input type="date" class="boton" id="fecha_es" name="fecha_es" required>
  </div>
    </div>
      </div>
          <input type="hidden" id="id" name="id" value="<?php echo $paciente; ?>" required>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button  class="btn btn-success" id="guardar_es" type="submit">Guardar</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
                                                                             <!--        fin de boton nuevo estudio-->
                                                                            <!--  inicio tabla estudio -->
<p></p>
<table id="TablaDeEstudios" class="table table-striped ">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Estudio</th>
                <th>Costo</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Fecha</th>
                <th>Estudio</th>
                <th>Costo</th>
                <th>Estado</th>
              </tr>
            </tfoot>
        </table>
                                                                           <!--  fin de tabla estudio -->



    </div>
  </div>
  </div>
</div>
</div>



 <?php
} else {
    ?>

    <h3 class="mt-4">Pacientes registrados</h3>
<div class="row" id="tb-donaciones">


<div class="card">


<!-- TABLA DE PACIENTES -->
<table id="TablaPacientes" class="table table-striped " style="margin-bottom: 3em;">
        <thead>
            <tr>
                <th>N#</th>
                <th>Paciente</th>
                <th>F/N</th>
                <th>Representante</th>
                <th>Tlf</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>N#</th>
                <th>Paciente</th>
                <th>F/N</th>
                <th>Representante</th>
                <th>Tlf</th>
            </tr>
        </tfoot>
    </table>

</div>


<br>
<br>
<br>

</div>
<?php
}
?>
<script src="../../librerias/js/js_de.js" type="text/javascript" charset="utf-8" async defer></script>

 <?php
require_once "../../default/footer.php";
?>



