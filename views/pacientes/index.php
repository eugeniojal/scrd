
<?php
include '../../default/header.php';
?>
<h3 class="mt-4">Registro de pacientes</h3>
                        <!-- <h3 class="mt-4" style="margin-left: 20px; color: #4b4c4e">Registro de pacientes</h3> -->

<div class="row" id="rgt-pacientes" >


<div class="card  mb-3 p-3 col-sm-12">


<form class="row g-3 needs-validation "   id="nuevo_pa" method="POST" action="../../api/GuardarCliente.php">




<!-- FORMULARIO REPRESENTANTE COLUMNA IZQUIERDA-->
<div class="left-reg text-center" style="background-image: url('../../recursos/ninos-copia.png');  background-size: 100%;  background-repeat: no-repeat;" >
  <div class="text-center" >
   <!-- <img class="fondoregistro" src="../../recursos/fondoregistro.jpg">-->
  <!--<img src="../../recursos/ninos-copia.png" class="modal__product" />-->

  </div>
            </div>



<!-- FORMULARIO REPRESENTANTE COLUMNA DERECHA-->

            <div class="right-reg text-center">
              <div class="row" style="margin-bottom: 2em;">



  </div>
              <label class="titulo-col-reg">Datos del representante</label>
              <div class="content">
                  <div class="col-sm-12 inputWithIcon ">
              <i class="fas fa-user-friends"></i>
              <label for="representante_pa" class="form-label"></label>
              <div class="input-group has-validation inputWithIcon">
                <input type="text" class="boton" id="representante_pa" name="representante_pa" aria-describedby="inputGroupPrepend" placeholder="Nombre y apellido"required>
                <div class="invalid-feedback">
                  Please choose a username.
                </div>
              </div>
            </div>
              <div class="col-sm-12 inputWithIcon">
                <i class="far fa-id-card"></i>
              <label for="cedula_re" class="form-label"></label>
              <div class="input-group has-validation">
                <input type="text" class="boton" id="cedula_re" name="cedula_re" aria-describedby="inputGroupPrepend" placeholder="Cédula" required>
                <div class="invalid-feedback">
                  Please choose a username.
                </div>
              </div>
            </div>
            <div class="col-sm-12 inputWithIcon">
              <i class="fas fa-map-marker-alt"></i>
              <label for="direccion_pa" class="form-label"></label>
               <div class="input-group has-validation">
              <input type="text" class="boton" id="direccion_pa" name="direccion_pa" placeholder="Dirección" >
              <div class="invalid-feedback">
                Please provide a valid city.
              </div>
              </div>
            </div>
            <div class="col-sm-12 inputWithIcon">
              <i class="fas fa-phone-alt"></i>
              <label for="tlf_pa" class="form-label"></label>
               <div class="input-group has-validation">
            <input type="text" class="boton" id="tlf_pa" name="tlf_pa" placeholder="Teléfono" required>
              <div class="invalid-feedback">
                Please select a valid state.
              </div>
               </div>
            </div>
           </div>
              <label class="titulo-col-reg">Datos del niño</label>
                <div class="content">
                   <div class="col-md-12 inputWithIcon">
                    <i class="fas fa-user"></i>
    <label for="nombre_pa" class="form-label"></label>
    <div class="input-group has-validation">
   <input type="text" class="boton" id="nombre_pa" name="nombre_pa" placeholder="Nombre y apellido " required>

    <div class="valid-feedback">
      Looks good!
    </div>
    </div>
  </div>
  <div class="col-md-12 inputWithIcon">
    <i class="far fa-calendar-alt"></i>
    <label for="edad_pa" class="form-label"></label>
    <div class="input-group has-validation"><div style="margin-left: 30px; margin-top:5px;text-transform: capitalize; color: #6d6d6d;">fecha de nac.</div>
    <input type="date"  class="input-date" id="edad_pa" name="edad_pa"  required >

    <div class="valid-feedback">
      Looks good!
    </div>
    </div>
  </div>


                  <div class="col-md-12 inputWithIcon " >
              <i class="fas fa-weight-hanging" ></i>
              <label for="Peso_pa" class="form-label"></label>
              <div class="input-group has-validation inputWithIcon">
                <input type="text" class="boton" id="Peso_pa" name="Peso_pa" aria-describedby="inputGroupPrepend" placeholder="Peso en kg"required>

              </div>
            </div>
                  <div class="col-md-12 inputWithIcon " >
              <i class="fas fa-hospital-alt" ></i>
              <label for="area_pa" class="form-label"></label>
              <div class="input-group has-validation inputWithIcon">
                <input type="text" class="boton" id="area_pa" name="area_pa" aria-describedby="inputGroupPrepend" placeholder="Area de atencion medica"required>
              </div>
            </div>



<div class="mb-12 inputWithIcon">
  <i class="fas fa-file-alt"></i>
  <!--<label for="diag_pa" class="boton-text-area"></label>-->
  <textarea class="boton-text-area" id="diag_pa" name="diag_pa" rows="3" placeholder="Diagnóstico"></textarea>
</div>

                </div>
                <div class="titulo-col-reg text-center">
    <button class="btn btn-primary boton_enviar" id="guardar_pa" type="submit">Guardar paciente</button>
  </div>
            </div>



</form>
</div>
</div>

<div class="row" id="tb-donaciones">


<div class="card">
   <center><h5 style="margin-bottom: 20px; margin-top: 20px;">LISTADO DE PACIENTES</h5></center>

<!-- TABLA DE PACIENTES -->
<table id="TablaPacientes" class="table table-striped " style="margin-bottom: 3em;">
        <thead>
            <tr>
                <th>N#</th>
                <th>Paciente</th>
                <th>F/N</th>
                <th>Representante</th>
                <th>Tlf</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>N#</th>
                <th>Paciente</th>
                <th>F/N</th>
                <th>Representante</th>
                <th>Tlf</th>
            </tr>
        </tfoot>
    </table>

</div>


<br>
<br>
<br>

</div>

<script src="../../librerias/js/js_pa.js" type="text/javascript" charset="utf-8" async defer></script>
<?php
include '../../default/footer.php';
?>