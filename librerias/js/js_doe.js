$(document).ready(function() {
   TablaDeDonaciones();
  $("#nav-don").addClass("active");
  $("#nav-pac").removeClass("active");
$.getJSON("../../api/pacientes2.php",function(datos){
                if(datos != 0){
                    var $SELECT2 = $("select#mySelect2").select2({
                      dropdownParent: $('#exampleModal'),
                    placeholder: 'Seleccione un Paciente',
                    // selectOnClose:'false',
                    // tema : "clásico",
                     width : 'resolve'
                    });
                    $.each(datos,function(K,V){
                        //console.log(V);
                        $SELECT2.append($('<option>', { //agrego los valores que obtengo de una base de datos
                            value: V['id_pa'],
                            text: V['nombre_pa']
                        }));
                    });
    
                }else{
                    //
                    //console.log('v');
                }
            });


$("#costo_do").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});


  $('#nueva_do').submit(function (ev) {
  $.ajax({
    type: $('#nueva_do').attr('method'), 
    url: $('#nueva_do').attr('action'),
    data: $('#nueva_do').serialize(),
    success: function (data) {


      alertify.set('notifier','position', 'bottom-center'); 

      if (parseInt(data) == 1) {
      alertify.success('Registro Exitoso').dismissOthers(); 
      document.getElementById("nueva_do").reset();
      TablaDeDonaciones();
       $('.modal-content').modal('hide');
      }else if(parseInt(data) == 2){
      alertify.error('Debe seleccionar un niño').dismissOthers(); 
      
      }else{

      alertify.error('Error en Registro').dismissOthers(); 
      }
      } 
  });
  ev.preventDefault();
});



});

function TablaDeDonaciones(){
 responsive: true;

  $('#TablaDonaciones').DataTable({
                rowReorder: {
            selector: 'td:nth-child(2)'
        },
   "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      "ajax": {
      "url": "../../api/MostrarDonacionese.php",
      "type": "GET",
      "dataSrc":"",
      }, 
      "columns": [{
         data: "fecha",
         render: $.fn.dataTable.render.moment('DD/MM/YYYY')
              },
     {
          data: "nombre_pa"
              }
              ,
     {
          data: "donacion"
              }
              ,
     {
          data: "costo_do",
          render: $.fn.dataTable.render.number( ',', '.', 2 )
              } ,
     {
          data: "estado_do",
             render : function ( data, type, full, meta ) {
              if(full.estado_do == 1){
                return "<span style='color:#1CC789;'>ENTREGADO</span>";
              }else{
                return "<span style='color:#F4B317;'>PENDIENTE</span>";
              }
            }
      }
       ]

   });

}