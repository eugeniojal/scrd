$(document).ready(function() {

  $("#nav-det").addClass("active");
  $("#nav-pac").removeClass("active");


if ($('#id_paciente').val() > 0 ) {

  var id = parseInt($('#id_paciente').val()) ;
  $.getJSON("../../api/DatosUsuario.php",{id:id},function(datos){
                  
                  $.each(datos,function(K,V){
                    var fecha = V['fn_pa'];
                    var fechaA = V['fecha_alta'];
                    fecha = (moment(fecha).format('DD/MM/YYYY'));
                    fechaA = (moment(fechaA).format('DD/MM/YYYY'));
                   // console.log(fecha);
                       $('#nombre_pa').html('Nombre: '+V['nombre_pa']);
                       $('#peso_pa').html('Peso: '+V['peso']+' Kg');
                       $('#nombre_re').html('<strong>Nombre: </strong>'+V['representante_pa']);
                       $('#cedula_re').html('<strong>Cedula: </strong>'+V['cedula_pa']);
                       $('#dir_re').html('<strong>Direccion: </strong>'+V['dir_pa']);
                       $('#area_pa').html('<strong>Area medica: </strong>'+V['area']);
                       $('#diag_pa').html('<strong>Diagnostico: </strong>'+V['diagnostico_pa']);

                tPhone= V['tlf_pa'].toString();            
                tPhone='(' + tPhone.substring(0,4) + ')' + tPhone.substring(4,7) + '-' + tPhone.substring(7,11);
                       $('#tlf_re').html('<strong>Tlf: </strong>'+tPhone);
                      switch(parseInt(V['estado_pa'])) {
                      case 1:

                     $("#flexSwitchCheckChecked").prop('checked', true);
                      var div = document.getElementById("text");
                      $("#text").empty();
                      $("#text").append("paciente activo");
                      div.style.color = "green";
                      $("#btn-estudio").prop('disabled', false);
                      $("#btn-donacion").prop('disabled', false);
                      break;
                      case 2:
                    $("#flexSwitchCheckChecked").prop('checked', false);
                      var div = document.getElementById("text");
                      $("#text").empty();
                      $("#text").append("paciente de alta el día:"+fechaA);
                      div.style.color = "red";
                      $("#btn-estudio").prop('disabled', true);
                      $("#btn-donacion").prop('disabled', true);
                      break;
                      default:

                    }
  

                                       
                       $('#fecha_pa').html(fecha);
                   });
    
  });


TablaDeDonaciones();
TablaDeEstudios();

$('#flexSwitchCheckChecked').on('click', function () {
    if( $('#flexSwitchCheckChecked').is(':checked') ) {

      alertify.confirm('Confirme por favor', 'Estas Seguro que desea activar este paciente?', function(){ 


              $.ajax({
    type: 'POST', 
    url: '../../api/CambioEstado.php',
    data: 'id='+id+'&Estado='+1,
    success: function (data) {
            $("#text").empty();
           $("#text").append("paciente activo");
           var div = document.getElementById("text");
            div.style.color = "green";
            $("#btn-estudio").prop('disabled', false);
            $("#btn-donacion").prop('disabled', false);
      } 
  });



       }, function(){ 
     $("#flexSwitchCheckChecked").prop('checked', false);

    }).set('labels', {ok:'Activar!', cancel:'Cancelar!'});

} else{



  alertify.confirm('Confirme por favor', 'Estas Seguro que desea desactivar este paciente?', function(){ 

   $.ajax({
    type: 'POST', 
    url: '../../api/CambioEstado.php',
    data:'id='+id+'&Estado='+2,
    success: function (data) {
            $("#text").empty();
            $("#text").append("paciente de alta");
            var div = document.getElementById("text");
            div.style.color = "red";
            $("#btn-estudio").prop('disabled', true);
            $("#btn-donacion").prop('disabled', true);
      } 
  });

     }
                , function(){ 


                  
$("#flexSwitchCheckChecked").prop('checked', true);




                }).set('labels', {ok:'Desactivar!', cancel:'Cancelar!'});


}
});

$(".mask").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});



  $('#nueva_do').submit(function (ev) {
  $.ajax({
    type: $('#nueva_do').attr('method'), 
    url: $('#nueva_do').attr('action'),
    data: $('#nueva_do').serialize(),
    success: function (data) {


      alertify.set('notifier','position', 'bottom-center'); 

      if (parseInt(data) == 1) {
      alertify.success('Registro Exitoso').dismissOthers(); 
      document.getElementById("nueva_do").reset();
      TablaDeDonaciones();
      }else if(parseInt(data) == 2){
      alertify.error('Debe seleccionar un niño').dismissOthers(); 
      
      }else{

      alertify.error('Error en Registro').dismissOthers(); 
      }
      } 
  });
  ev.preventDefault();
});

  $('#nueva_es').submit(function (ev) {
  $.ajax({
    type: $('#nueva_es').attr('method'), 
    url: $('#nueva_es').attr('action'),
    data: $('#nueva_es').serialize(),
    success: function (data) {
      alertify.set('notifier','position', 'bottom-center'); 
      if (parseInt(data) == 1) {
      alertify.success('Registro Exitoso').dismissOthers(); 
      document.getElementById("nueva_es").reset();
      TablaDeEstudios();
      }else if(parseInt(data) == 2){
      
      }else{

      alertify.error('Error en Registro').dismissOthers(); 
      }
      } 
  });
  ev.preventDefault();
});


}else{
TablaDePacientes()
}

});

function TablaDeDonaciones(){
 responsive: true;
var id = $('#id_paciente').val();
  $('#TablaDonaciones').DataTable({
                rowReorder: {
            selector: 'td:nth-child(2)'
        },
   "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      "ajax": {
      "url": "../../api/MostrarDonacionesD.php",
      "data":{"id":id},
      "type": "GET",
      "dataSrc":"",
      }, 
      "columns": [{
         data: "fecha",
         render: $.fn.dataTable.render.moment('DD/MM/YYYY')
              },
             {
          data: "donacion"
              }
              ,
     {
          data: "costo_do",
          render: $.fn.dataTable.render.number( ',', '.', 2 )
              }
       ]

   });

}
function TablaDeEstudios(){
 responsive: true;
var id = $('#id_paciente').val();
  $('#TablaDeEstudios').DataTable({
                rowReorder: {
            selector: 'td:nth-child(2)'
        },
   "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      "ajax": {
      "url": "../../api/MostrarEstudiosD.php",
      "data":{"id":id},
      "type": "GET",
      "dataSrc":"",
      }, 
      "columns": [{
         data: "fecha",
         render: $.fn.dataTable.render.moment('DD/MM/YYYY')
              },
             {
          data: "nombre_es"
              }
              ,
     {
          data: "costo",
          render: $.fn.dataTable.render.number( ',', '.', 2 )
              }              ,
     {
          data: "estatus_es",
      
              }
       ]

   });

}

function TablaDePacientes(){


  $('#TablaPacientes').DataTable({   
            rowReorder: {
            selector: 'td:nth-child(2)'
        },
   "language": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ registros",
         "sZeroRecords": "No se encontraron resultados",
         "sEmptyTable": "Ningún dato disponible en esta tabla",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
         "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
         "sInfoPostFix": "",
         "sSearch": "Buscar:",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
       // responsive: true,
      "destroy": true,
      "responsive": true,
      "paging": true,
      "autoWidth": false,
      "ajax": {
      "url": "../../api/MostrarPacientes.php",
      "type": "GET",
      "dataSrc":"",
      },
      "columns": [
           {
          data: "id_pa"
              },
      {
         data: "nombre_pa",
                render : function ( data, type, full, meta ) {
                return "<a href='../DetallePaciente/index.php?paciente="+full.id_pa+"' >"+full.nombre_pa+" </a>";
             }
              },
     {
          data: "fn_pa",
          render: $.fn.dataTable.render.moment('DD/MM/YYYY')
              }
              ,
     {
          data: "representante_pa"
              },
     {
          data: "tlf_pa",
          render: function ( toFormat ) {
                var tPhone;
                tPhone=toFormat.toString();            
                tPhone='(' + tPhone.substring(0,4) + ')' + tPhone.substring(4,7) + '-' + tPhone.substring(7,11);   
                return tPhone }
              }  

       ]
   });

}