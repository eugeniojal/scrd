$(document).ready(function(){


  $.getJSON("../../api/dashboard.php",function(datos){
                  
                  $.each(datos,function(K,V){
                  	$('#pa').html(V['pacientes']);
                  	$('#de').html(V['donaciones1']);
                  	$('#dp').html(V['donaciones2']);
                  	$('#es').html(V['estudios']);
                   });
    
  });

  $.getJSON("../../api/estadisticas.php?est=1",function(datos){
                  

      var score = [];
      var estudios = [];
	const labels = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];


                 $.each(datos,function(K,V){
                 	score.push(V['1']);
                 	score.push(V['2']);
                 	score.push(V['3']);
                 	score.push(V['4']);
                 	score.push(V['5']);
                 	score.push(V['6']);
                 	score.push(V['7']);
                 	score.push(V['8']);
                 	score.push(V['9']);
                 	score.push(V['10']);
                 	score.push(V['11']);
                 	score.push(V['12']);

                 	estudios.push(V['1e']);
                 	estudios.push(V['2e']);
                 	estudios.push(V['3e']);
                 	estudios.push(V['4e']);
                 	estudios.push(V['5e']);
                 	estudios.push(V['6e']);
                 	estudios.push(V['7e']);
                 	estudios.push(V['8e']);
                 	estudios.push(V['9e']);
                 	estudios.push(V['10e']);
                 	estudios.push(V['11e']);
                 	estudios.push(V['12e']);
                   });

// console.log(score);
const data = {
  labels: labels,
  datasets: [{
    label: 'Donaciones entregadas',
    backgroundColor: 'rgb(255,177,193)',
    borderColor: 'rgb(38,206,76)',
    data: score,
  },
  {
    label: 'Estudios asignados',
    backgroundColor: 'rgb(154,208,245)',
    borderColor: 'rgb(38,206,76)',
    data: estudios,
  }]
};
const config = {
  type: 'bar',
  data,
  options: {}
};

  var myChart = new Chart(
    document.getElementById('myChart'),
    config
  );
   
  });

  $.getJSON("../../api/estadisticas.php?est=2",function(datos){
                  

      var score = [];
	const labels = [
  'Ingresos',
  'Salidas',

];


                 $.each(datos,function(K,V){
                 	score.push(V['ingresos']);
                 	score.push(V['salida']);

                   });

const data = {
  labels: labels,
  datasets: [{
    label: 'Donaciones entregadas',
    backgroundColor: [
    'rgb(54, 162, 235)',
       'rgb(255, 99, 132)'
      ],
    data: score,
  }]
};
const config = {
  type: 'pie',
  data,
  options: {}
};

  var myChart = new Chart(
    document.getElementById('myChart1'),
    config
  );
   
  });
    


$.getJSON("../../api/estadisticas.php?est=3",function(datos){

var score = [];
const labels = [
  'Entregadas',
  'Pendientes',
];


 $.each(datos,function(K,V){
 	score.push(V['entregadas']);
 	score.push(V['pendientes']);
   });

// console.log(score);
const data = {
  labels: labels,
  datasets: [{
    label: 'Donaciones entregadas y pendientes',
    backgroundColor: [
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)'
    ],
    data: score,
  }]
};
const config = {
  type: 'doughnut',
  data,
  options: {}
};

  var myChart = new Chart(
    document.getElementById('myChart2'),
    config
  );
   
  });




  });
