-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2021 a las 14:33:51
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `scrd`
--
CREATE DATABASE IF NOT EXISTS `scrd` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `scrd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE `diagnostico` (
  `id_di` int(10) NOT NULL,
  `nombre_di` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_pa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donaciones`
--

CREATE TABLE `donaciones` (
  `id_do` int(10) NOT NULL,
  `id_pa` int(10) NOT NULL,
  `donacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `costo_do` double NOT NULL,
  `fecha` date NOT NULL,
  `estado_do` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudios`
--

CREATE TABLE `estudios` (
  `id_es` int(10) NOT NULL,
  `nombre_es` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `costo` int(11) NOT NULL,
  `id_pa` int(11) NOT NULL,
  `estatus_es` int(1) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id_pa` int(10) NOT NULL,
  `nombre_pa` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `edad_pa` int(5) NOT NULL,
  `dir_pa` text COLLATE utf8_spanish_ci NOT NULL,
  `representante_pa` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `img` longblob NOT NULL,
  `diagnostico_pa` text COLLATE utf8_spanish_ci NOT NULL,
  `cedula_pa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tlf_pa` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fn_pa` date NOT NULL,
  `fecha_reg` date NOT NULL,
  `estado_pa` int(1) NOT NULL DEFAULT '1',
  `peso` double NOT NULL,
  `area` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_alta` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representante`
--

CREATE TABLE `representante` (
  `id_re` int(10) NOT NULL,
  `nombre_re` varchar(22) COLLATE utf8_spanish_ci NOT NULL,
  `ci_re` int(20) NOT NULL,
  `tlf_re` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `id_pa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_us` int(10) NOT NULL,
  `nombre_us` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_us` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pass_us` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nivel_us` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_us`, `nombre_us`, `usuario_us`, `pass_us`, `nivel_us`) VALUES
(1, 'administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD PRIMARY KEY (`id_di`),
  ADD KEY `id_pa` (`id_pa`);

--
-- Indices de la tabla `donaciones`
--
ALTER TABLE `donaciones`
  ADD PRIMARY KEY (`id_do`),
  ADD KEY `id_pa` (`id_pa`);

--
-- Indices de la tabla `estudios`
--
ALTER TABLE `estudios`
  ADD PRIMARY KEY (`id_es`),
  ADD KEY `id_pa` (`id_pa`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_pa`);

--
-- Indices de la tabla `representante`
--
ALTER TABLE `representante`
  ADD PRIMARY KEY (`id_re`),
  ADD KEY `id_pa` (`id_pa`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_us`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  MODIFY `id_di` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `donaciones`
--
ALTER TABLE `donaciones`
  MODIFY `id_do` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estudios`
--
ALTER TABLE `estudios`
  MODIFY `id_es` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id_pa` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `representante`
--
ALTER TABLE `representante`
  MODIFY `id_re` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_us` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`id_pa`) REFERENCES `paciente` (`id_pa`);

--
-- Filtros para la tabla `donaciones`
--
ALTER TABLE `donaciones`
  ADD CONSTRAINT `donaciones_ibfk_1` FOREIGN KEY (`id_pa`) REFERENCES `paciente` (`id_pa`);

--
-- Filtros para la tabla `estudios`
--
ALTER TABLE `estudios`
  ADD CONSTRAINT `estudios_ibfk_1` FOREIGN KEY (`id_pa`) REFERENCES `paciente` (`id_pa`);

--
-- Filtros para la tabla `representante`
--
ALTER TABLE `representante`
  ADD CONSTRAINT `representante_ibfk_1` FOREIGN KEY (`id_pa`) REFERENCES `paciente` (`id_pa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
