<?php
require('../librerias/fpdf/fpdf.php');
require_once('../default/conexion.php');
$model = new conexion();

$reporte = $_REQUEST['reporte'];

switch ($reporte) {
	case 1:
		
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('DONACIONES ENTREGADAS ');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"REPORTE DONACIONES ENTREGADAS ");

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DONACION ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'MONTO ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'FECHA ');


$query = "SELECT nombre_pa,donacion,costo_do,fecha,estado_do
FROM donaciones
INNER JOIN paciente
ON donaciones.id_pa = paciente.id_pa  
WHERE estado_do = 1";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
 foreach ($datosRecibidos as $key) {
	$totaldis = $totaldis + $key['costo_do'];
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	  $pdf->Cell(60, 8, $key['donacion'], 0);
  
	$pdf->Cell(50,8,number_format($key['costo_do'],2,',','.'), 0);
	$fecha = date_create($key['fecha']);
  $pdf->Cell(60, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total:'.number_format($totaldis,2,',','.'),0);
$pdf->Output('REPORTE DE DONACIONES ENTREGADAS.pdf','I');





		break;
case 2:
		
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('DONACIONES ENTREGADAS ');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"REPORTE DONACIONES PENDIENTES ");

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DONACION ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'MONTO ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'FECHA ');


$query = "SELECT nombre_pa,donacion,costo_do,fecha,estado_do
FROM donaciones
INNER JOIN paciente
ON donaciones.id_pa = paciente.id_pa  
WHERE estado_do = 0";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
 foreach ($datosRecibidos as $key) {
	$totaldis = $totaldis + $key['costo_do'];
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	  $pdf->Cell(60, 8, $key['donacion'], 0);
  
	$pdf->Cell(50,8,number_format($key['costo_do'],2,',','.'), 0);
	$fecha = date_create($key['fecha']);
  $pdf->Cell(60, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total:'.number_format($totaldis,2,',','.'),0);
$pdf->Output('REPORTE DE DONACIONES PENDIENTES.pdf','I');

break;

case 3:

$id_pa = $_REQUEST['us'];
$query = "SELECT nombre_pa
FROM paciente
WHERE id_pa = '$id_pa' ";
$datosRecibidos = $model->obtenerDatos($query);
 foreach ($datosRecibidos as $key) {
$paciente = $key['nombre_pa'];
}
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('DONACIONES A '.$paciente);
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"DONACIONES A ".$paciente);

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'DONACION ');
$pdf->SetXY(70, 70);
$pdf->Cell(20,5,'MONTO ');
$pdf->SetXY(120, 70);
$pdf->Cell(20,5,'FECHA ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'ESTATUS ');


$query = "SELECT donacion,costo_do,fecha,estado_do
FROM donaciones
WHERE id_pa = '$id_pa' ";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
 foreach ($datosRecibidos as $key) {
	$totaldis = $totaldis + $key['costo_do'];
	  $pdf->Cell(60, 8, $key['donacion'], 0);
	$pdf->Cell(50,8,number_format($key['costo_do'],2,',','.'), 0);
  	$fecha = date_create($key['fecha']);
  $pdf->Cell(40, 8, date_format($fecha,'d-m-Y'), 0);
  if ($key['estado_do'] == 1) {
  	$pdf->Cell(60, 8, "ENTREGADA", 0);
  }else{
  	$pdf->Cell(60, 8, "PENDIENTE", 0);
  }
  
	$pdf->Ln(4);
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50,8,'',0);
$pdf->Cell(80,8,'Total:'.number_format($totaldis,2,',','.'),0);
$pdf->Output('DONACIONES A '.$paciente.'.pdf','I');
break;

case 4:


$desde = $_REQUEST['desde'];
$hasta = $_REQUEST['hasta'];
$desde1 = date_create($_REQUEST['desde']);
$hasta1 = date_create($_REQUEST['hasta']);
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('DONACIONES POR FECHA ');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(50, 15);
$pdf->Cell(30,5,"DONACIONES DESDE:".date_format($desde1,'d-m-Y').' HASTA:'.date_format($hasta1,'d-m-Y'));

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DONACION ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'MONTO ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'FECHA ');


$query = "SELECT nombre_pa,donacion,costo_do,fecha,estado_do
FROM donaciones
INNER JOIN paciente
ON donaciones.id_pa = paciente.id_pa  
WHERE fecha BETWEEN '$desde' AND '$hasta'";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
 foreach ($datosRecibidos as $key) {
	$totaldis = $totaldis + $key['costo_do'];
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	  $pdf->Cell(60, 8, $key['donacion'], 0);
  
	$pdf->Cell(50,8,number_format($key['costo_do'],2,',','.'), 0);
	$fecha = date_create($key['fecha']);
  $pdf->Cell(60, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total:'.number_format($totaldis,2,',','.'),0);
$pdf->Output('REPORTE POR FECHA.pdf','I');

break;
case 5:
		
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('REPORTE DE PACIENTES ACTIVOS');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"REPORTE DE PACIENTES ACTIVOS ");

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DIAGNOSTICO ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'TLF ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'FECHA REGISTRO ');


$query = "SELECT * FROM paciente WHERE estado_pa = 1";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
   $i = 0;
 foreach ($datosRecibidos as $key) {
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	$pdf->Cell(60, 8, $key['diagnostico_pa'], 0);
	$pdf->Cell(50, 8, $key['tlf_pa'], 0);
	$fecha = date_create($key['fecha_reg']);
    $pdf->Cell(60, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
	$i++;
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
// $pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total Pacientes: '.$i,0);
$pdf->Output('REPORTE DE PACIENTES ACTIVOS.pdf','I');



break;
case 6:
		
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('REPORTE DE PACIENTES INACTIVOS');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"REPORTE DE PACIENTES INACTIVOS ");

$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DIAGNOSTICO ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'TLF ');
$pdf->SetXY(125,70);
$pdf->Cell(20,5,'FECHA REGISTRO ');
$pdf->SetXY(165,70);
$pdf->Cell(20,5,'FECHA SALIDA ');


$query = "SELECT * FROM paciente WHERE estado_pa = 2";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
   $i = 0;
 foreach ($datosRecibidos as $key) {
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	$pdf->Cell(55, 8, $key['diagnostico_pa'], 0);
	$pdf->Cell(30, 8, $key['tlf_pa'], 0);
	$fecha = date_create($key['fecha_reg']);
    $pdf->Cell(35, 8, date_format($fecha,'d-m-Y'), 0);
    $fecha = date_create($key['fecha_alta']);
    $pdf->Cell(35, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
	$i++;
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
// $pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total Pacientes: '.$i,0);
$pdf->Output('REPORTE DE PACIENTES INACTIVOS.pdf','I');



break;
case 7:
$desde = $_REQUEST['desde'];
$hasta = $_REQUEST['hasta'];
$desde1 = date_create($_REQUEST['desde']);
$hasta1 = date_create($_REQUEST['hasta']);

$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetTitle('REPORTE DE PACIENTES POR FECHA');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(160, 8);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(70, 15);
$pdf->Cell(30,5,"REPORTE DE PACIENTES POR FECHA ");
$pdf->SetXY(72, 25);
$pdf->Cell(30,8,"DESDE:".date_format($desde1,'d-m-Y')." HASTA:".date_format($hasta1,'d-m-Y'));
$pdf->SetXY(10, 70);
$pdf->Cell(20,5,'PACIENTE ');
$pdf->SetXY(50, 70);
$pdf->Cell(20,5,'DIAGNOSTICO ');
$pdf->SetXY(110, 70);
$pdf->Cell(20,5,'TLF ');
$pdf->SetXY(160,70);
$pdf->Cell(20,5,'FECHA REGISTRO ');


$query = "SELECT * FROM paciente WHERE fecha_reg BETWEEN '$desde' AND '$hasta'";
$datosRecibidos = $model->obtenerDatos($query);

  $totaldis = 0;
  $pdf->SetXY(10, 80);
  $pdf->SetFont('Arial', 'B', 8);
   $i = 0;
 foreach ($datosRecibidos as $key) {
    $pdf->Cell(40, 8,$key['nombre_pa'], 0);
	$pdf->Cell(60, 8, $key['diagnostico_pa'], 0);
	$pdf->Cell(50, 8, $key['tlf_pa'], 0);
	$fecha = date_create($key['fecha_reg']);
    $pdf->Cell(60, 8, date_format($fecha,'d-m-Y'), 0);
	$pdf->Ln(4);
	$i++;
}
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
// $pdf->Cell(100,8,'',0);
$pdf->Cell(80,8,'Total Pacientes: '.$i,0);
$pdf->Output('REPORTE DE PACIENTES INACTIVOS.pdf','I');



break;
	default:
		# code...
		break;
}










?>
